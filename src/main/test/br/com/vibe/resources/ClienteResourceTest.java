package br.com.vibe.resources;

import br.com.vibe.models.domains.Cliente;
import br.com.vibe.models.domains.Cupom;
import br.com.vibe.models.services.contracts.ClienteService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import util.TestUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ClienteResourceTest {

    private Cliente cliente;

    @InjectMocks
    private ClienteResource clienteResource;

    @Mock
    private ClienteService clienteService;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(clienteResource).build();
    }


    @Test
    public void testShouldReturnClienteByCpfOrEmail() throws Exception {

        cliente = getClienteMockUp();

        when(clienteService.searchClienteByCpfOrEmail(anyString(), anyString()))
                .thenReturn(cliente);

        mockMvc.perform(
                get("/Cliente/00989009887")
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testShouldReturnParameterExceptionError() throws Exception {

        mockMvc.perform(
                get("/Cliente/009.890.098-87")
        )
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN))
                .andExpect(content().string("Cliente n&atilde;o encontrado. Efetuar Registro."));

    }


    @Test
    public void testShouldSaveNewCliente() throws Exception {

        cliente = getClienteMockUp();
        cliente.setId(1);
        cliente.setCupons(new ArrayList<Cupom>());

        when(clienteService.saveCliente(anyObject()))
                    .thenReturn(cliente);


        mockMvc.perform(
                post("/Cliente")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.conertObjectToJsonBytes(cliente))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id",  is(1)))
                .andExpect(jsonPath("$.Nome", is("David Willian")))
                .andExpect(jsonPath("$.Cpf", is("00989009887")))
                .andExpect(jsonPath("$.Email", is("david@teste.com")))
                .andExpect(jsonPath("$.Cupons", hasSize(0)))
                .andExpect(jsonPath("$.QuantidadeCupons", is(0)));

    }

    @Test
    public void testShouldReturnClienteById() throws Exception {

        Cupom cupom = new Cupom();
        Cupom cupom1 = new Cupom();

        cupom.setData(Calendar.getInstance());
        cupom.setNumero("codig-2923-aleatorio");

        cupom1.setData(Calendar.getInstance());
        cupom1.setNumero("codig-23232-aleatorio");


        cliente = getClienteMockUp();
        cliente.setId(1);
        cliente.setCupons(
                Arrays.asList(cupom, cupom1)
        );

        when(clienteService.searchClienteById(anyInt()))
                    .thenReturn(cliente);

        mockMvc.perform(
                put("/Cliente/1")
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$.id",  is(1)))
               .andExpect(jsonPath("$.Nome", is("David Willian")))
               .andExpect(jsonPath("$.Cpf", is("00989009887")))
               .andExpect(jsonPath("$.Email", is("david@teste.com")))
               .andExpect(jsonPath("$.Cupons", hasSize(2)))
               .andExpect(jsonPath("$.QuantidadeCupons", is(2)));

    }

    @Test
    public void testShouldNotExistCliente() throws Exception {

        when(clienteService.searchClienteById(anyInt()))
                   .thenReturn(null);

        mockMvc.perform(
                put("/Cliente/1")
        )
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.TEXT_PLAIN))
                .andExpect(content().string("Cliente n&atilde;o encontrado."));

    }


    private Cliente getClienteMockUp(){

       Cliente cliente = new Cliente();

        cliente.setNome("David Willian");
        cliente.setCpf("00989009887");
        cliente.setEmail("david@teste.com");

       return cliente;
    }

}