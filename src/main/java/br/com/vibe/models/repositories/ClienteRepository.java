package br.com.vibe.models.repositories;

import br.com.vibe.models.domains.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("SELECT c FROM Cliente c JOIN c.cupons cp WHERE c.email = :email")
    Cliente findByEmail(@Param("email") String email);

    @Query("SELECT c FROM Cliente c JOIN FETCH c.cupons cp WHERE c.cpf = :cpf")
    Cliente findByCpf(@Param("cpf") String cpf);
}
