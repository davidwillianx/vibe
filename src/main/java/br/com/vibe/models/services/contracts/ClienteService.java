package br.com.vibe.models.services.contracts;

import br.com.vibe.models.domains.Cliente;

public interface ClienteService {

    Cliente searchClienteByCpfOrEmail(String emailOrCpfAttribute, String requestPath);
    Cliente saveCliente(Cliente newCliente);
    Cliente searchClienteById(int clienteId);
    void removeClienteById(int clienteId);

}
