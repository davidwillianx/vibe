package br.com.vibe.models.services;

import br.com.vibe.models.domains.Cliente;
import br.com.vibe.models.repositories.ClienteRepository;
import br.com.vibe.models.services.contracts.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class ClienteServiceImpl implements ClienteService {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String CPF_PATTERN = "^[0-9]{11}$";
    private Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private Pattern cpfPattern = Pattern.compile(CPF_PATTERN);

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente searchClienteByCpfOrEmail(String emailOrCpfAttribute, String requestPath) {

        String email;
        String cpf = emailOrCpfAttribute;

        if( cpfPattern.matcher(emailOrCpfAttribute).matches() )
            return clienteRepository.findByCpf(cpf);

        String[] urlAttributes = requestPath.split("/");

        if(urlAttributes.length == 4){
            email = urlAttributes[3];

            if(emailPattern.matcher(email).matches())
                return clienteRepository.findByEmail(email);
        }


        throw new IllegalArgumentException();
    }

    @Override
    public Cliente saveCliente(Cliente newCliente) {

       Cliente clienteSaved = clienteRepository.save(newCliente);

       return clienteSaved;
    }

    public Cliente searchClienteById(int clienteId){

      Cliente clienteFound = clienteRepository.findOne(clienteId);

      return clienteFound;
    }

    @Override
    public void removeClienteById(int clienteId) {
         clienteRepository.delete(clienteId);
    }
}
