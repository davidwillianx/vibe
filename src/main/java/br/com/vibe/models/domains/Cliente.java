package br.com.vibe.models.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "viber_cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty("Id")
    private int id;

    @NotNull(message = "Nome do Cliente inv&aacute;lido")
    @NotBlank(message = "Nome do Cliente inv&aacute;lido")
    @JsonProperty("Nome")
    private String nome;

    @NotNull
    @Pattern(regexp = "^[0-9]{11}$", message = "Cpf do Cliente inv&aacute;lido")
    @JsonProperty("Cpf")
    private String cpf;

    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "Email do Cliente inv&aacute;lido")
    @JsonProperty("Email")
    private String email;

    @Transient
    @JsonProperty("QuantidadeCupons")
    private int quantidadeCupons;


    @JsonProperty("Cupons")
    @OneToMany(fetch = FetchType.EAGER)
    private List<Cupom> cupons = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuantidadeCupons(int quantidadeCupons) {
        this.quantidadeCupons = quantidadeCupons;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cupom> getCupons() {
        return cupons;
    }

    public void setCupons(List<Cupom> cupons) {
        this.cupons = cupons;
    }

    public int getQuantidadeCupons(){
        quantidadeCupons = cupons.size();
        return quantidadeCupons;
    }
}
