package br.com.vibe.resources;

import br.com.vibe.models.domains.Cliente;
import br.com.vibe.models.services.contracts.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/Cliente")
public class ClienteResource {


    @Autowired
    private ClienteService clienteService;

    @RequestMapping(value =  "/{emailOrCpf}")
    public ResponseEntity clienteByCpfOrEmail(@PathVariable("emailOrCpf") String emailOrCpf, HttpServletRequest request){

        String requestPath = request.getRequestURI();
        Cliente clienteFound = clienteService.searchClienteByCpfOrEmail(emailOrCpf, requestPath);

        if(clienteFound == null)
            return  ResponseEntity
                            .status(HttpStatus.NOT_FOUND)
                             .contentType(MediaType.TEXT_PLAIN)
                            .body("Cliente n&atilde;o encontrado. Efetuar Registro.");


        return new ResponseEntity(clienteFound, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity newCliente(@Valid @RequestBody Cliente newCliente){
      Cliente clienteSaved = clienteService.saveCliente(newCliente);
       return new ResponseEntity(clienteSaved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{clienteId}", method = RequestMethod.PUT)
    public ResponseEntity cliente(@PathVariable int clienteId){
       Cliente clienteFound =  clienteService.searchClienteById(clienteId);

        if(clienteFound == null)
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.TEXT_PLAIN)
                    .body("Cliente n&atilde;o encontrado.");

        return new ResponseEntity(clienteFound, HttpStatus.OK);

    }

    @RequestMapping( value = "/{clienteId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCliente(@PathVariable("clienteId") int clienteId){

        clienteService.removeClienteById(clienteId);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("OK");
    }

    @ExceptionHandler(DataAccessException.class)
    private ResponseEntity clienteEmptyResultDataAccessExceptionHandler(){
        return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("Cliente n&atilde;o encontrado");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity clienteNotAllowedFormatData(MethodArgumentNotValidException error){

        StringBuilder invalidFieldMessage = new StringBuilder();

        error.getBindingResult().getFieldErrors()
                .forEach( fieldError -> {
                    invalidFieldMessage.append(fieldError.getDefaultMessage().concat(" <br>"));

        });

        return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .contentType(MediaType.TEXT_PLAIN)
                .body(invalidFieldMessage.toString());

    }

}
