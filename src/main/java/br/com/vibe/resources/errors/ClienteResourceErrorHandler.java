package br.com.vibe.resources.errors;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ClienteResourceErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    public ResponseEntity paramErrorHandler(RuntimeException exc, WebRequest request){

        ObjectNode responseHandler = JsonNodeFactory.instance.objectNode();
        responseHandler.put("Message", " A solicita&ccedil;&atilde;o &eacute; inv&aacute;lida");
        //@TODO CHANGE MESSAGE
        responseHandler.put("MessageDetail", "O dicion&aacute;rio de par&acirc;metros cont&eacute;m uma entrada nula para o par&acirc;metro \"id\" de tipo que n&atilde;o permite valor anul&aacute;vel \"System.Int32\" para o m&eacute;todo \"VibeSelecao.Models.Cliente Put(Int32)\" em \"VibeSelecao.Controllers.CLienteController\" ");

        return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(responseHandler);
    }
}
