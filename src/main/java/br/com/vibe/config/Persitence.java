package br.com.vibe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

@Profile("prod")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.vibe.models.repositories")
public class Persitence {

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws URISyntaxException {

        LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setPackagesToScan("br.com.vibe.models.domains");
        factoryBean.setDataSource(dataSource());
        factoryBean.setJpaVendorAdapter( new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(additionalProperties());


        return factoryBean;
    }

    @Bean
    public DataSource dataSource() throws URISyntaxException {

        URI dbUri = new URI(System.getenv("DATABASE_URL"));
        String dbURL = "jdbc:postgresql://" + dbUri.getHost() + ":" + dbUri.getPort() + dbUri.getPath();
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        DriverManagerDataSource dataSource;

        dataSource = new DriverManagerDataSource(
                dbURL,
                username,
                password
        );

        dataSource.setDriverClassName("org.postgresql.Driver");

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){

        JpaTransactionManager jpaTransactionManager;
        jpaTransactionManager = new JpaTransactionManager();

        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);

        return jpaTransactionManager;
    }


    public Properties additionalProperties(){

        Properties additionalProperties;
        additionalProperties = new Properties();

        additionalProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        additionalProperties.setProperty("hibernate.hbm2ddl.auto", "update");
        additionalProperties.setProperty("hibernate.show_sql", "true");

        return additionalProperties;
    }
}
