package br.com.vibe.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Profile("dev")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.vibe.models.repositories")
public class PersistenceDev {


    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean factoryBean() throws IOException {

        LocalContainerEntityManagerFactoryBean factoryBean;

        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPackagesToScan("br.com.vibe.models.domains");
        factoryBean.setDataSource(dataSource());
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(additionalProperties());

        return factoryBean;
    }



    @Bean
    public DataSource dataSource() throws IOException {

        Resource databaseResource;
        Properties databaseProperties;
        DriverManagerDataSource dataSource;


        databaseResource = new ClassPathResource("./persistence.properties");
        databaseProperties = PropertiesLoaderUtils.loadProperties(databaseResource);

        dataSource = new DriverManagerDataSource(
                databaseProperties.getProperty("URL"),
                databaseProperties.getProperty("USERNAME"),
                databaseProperties.getProperty("PASSWORD")
        );

        //DRIVER PODE SER ALTERADO PARA MYSQL
        dataSource.setDriverClassName("org.postgresql.Driver");

        return dataSource;

    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){

        JpaTransactionManager transactionManager;

        transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);

        return transactionManager;
    }


    private Properties additionalProperties(){

        Properties additionalProperties;
        additionalProperties = new Properties();
        //DRIVER PODE SER ALTERADO PARA MYSQL
        additionalProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        additionalProperties.setProperty("hibernate.hbm2ddl.auto", "update");

        return additionalProperties;
    }

}
